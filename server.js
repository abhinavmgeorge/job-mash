require('dotenv').config()
const express = require('express')
const port = 5000
const app = express()
app.use(express.static('public'));

const mysql = require('mysql')
const con = mysql.createConnection({
  host: 'localhost',
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME
})

con.connect(err => {
	if(!err) {
		console.log('connected to db')
	}
	else {
		console.log(err)
	}
})


app.set('views', './views')
app.set('view engine', 'ejs')

const Probability = (rating1, rating2) => {

	return 1.0 * 1.0 / (1 + 1.0 * (10 ** (1.0 * (rating1 - rating2) / 400)))
}

const EloRating = (Ra, Rb, K, d) => {
    Pb = Probability(Ra, Rb)
  
    Pa = Probability(Rb, Ra)

	if(d == 1) {
        Ra = Ra + K * (1 - Pa)
        Rb = Rb + K * (0 - Pb)
	}else {
		Ra = Ra + K * (0 - Pa)
        Rb = Rb + K * (1 - Pb)
	}

	return [Ra, Rb]
}
 
app.get('/', (req,res) => {
	let randomNum1 = Math.floor(Math.random() * 71);
	let randomNum2 = Math.floor(Math.random() * 71);
	if(randomNum1==randomNum2){
		randomNum2 = Math.floor(Math.random() * 71);
	}
	con.query('SELECT * FROM jobs', (err, results) => {
		if(!err){
			res.render('home', {option1: results[randomNum1].title, option2: results[randomNum2].title})
		}
		else{
			console.log(err)
			res.render('home')
		}
	})
})

app.get('/top', (req,res) => {
	con.query('SELECT * FROM jobs ORDER BY rating DESC', (err, results) => {
		if(!err){
			res.render('top', {topList: results})
		}else{
			res.send(err)
		}		
	})
})

app.post('/result/:winner/:loser', (req,res) => {
	con.query(`SELECT rating FROM jobs WHERE title = "${req.params['winner']}"`, (err, winner) => {
		if(!err){
			let winnerRating = winner[0].rating;	
			con.query(`SELECT rating FROM jobs WHERE title = "${req.params['loser']}"`, (err, loser) => {
				if(!err){
					let loserRating = loser[0].rating;	
					const k = 30;
					const d = 1;
					const [winnerNewRating, loserNewRating] = EloRating(winnerRating, loserRating, k, d) 
					con.query(`UPDATE jobs SET rating = ${winnerNewRating} WHERE title = "${req.params['winner']}"`, err => {
						if(err){
							console.log(err)
						}
					})
					con.query(`UPDATE jobs SET rating = ${loserNewRating} WHERE title = "${req.params['loser']}"`, err => {
						if(err){
							console.log(err)
						}
					})
					res.redirect(`/`)
				}else{
					console.log(err)
				}
			})
		}else{
			console.log(err)
		}
	})
})
 
 
app.listen(port,() => console.log('app has opened'))

